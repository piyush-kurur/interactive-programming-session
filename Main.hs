module Main where
import Data.Char

main :: IO ()
main = do s <- getContents
          putStr (shout s)


shout :: String -> String
shout = map toUpper

listOfWords :: String -> String
listOfWords  = unlines . words

numberedList :: String -> String
numberedList s = unlines (zipWith combine [(1::Integer)..] (words s))
  where combine n str = unwords [show n, str]
