# Interactive Programming Session

This is the repository associated with an interactive programming
session that I did for the first year Computer Science Students at IIT
Palakkad. I also have a desktop record of the session (audio is pretty
poor) available at
https://vimeo.com/piyushkurur/interactive-programming-session

We demonstrate some simple Haskell program but make use of the emacs
editor (intero+stack for Haskell), git (mainly through magit).

There is also a presentation file in presentation.org
